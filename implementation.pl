% Leonardo de Sousa Rodrigues, RA: 220063

% --- INTERSECTIONS -----------------------------------------

% True if the intervals [A1, A2] and [B1, B2] intersect. Presume crescent order.
intersect_interval(A1, A2, B1, B2) :-
    (A1 < B1 -> A2 >= B1; A1 =< B2).

% True if rectangles have some area of intersection.
intersect(ret(_, X1a, Y1a, X2a, Y2a), ret(_, X1b, Y1b, X2b, Y2b)) :-
    intersect_interval(X1a, X2a, X1b, X2b),
    intersect_interval(Y1a, Y2a, Y1b, Y2b).

% Get a rectangle name.
name(ret(Name, _, _, _, _), Name).

% Get all intersections in a list of rectangles.
% Use name comparation to ensure different names and remove duplicates.
all_intersections(RetsIn, Pairs) :-
    findall((NameA, NameB), (
        member(RecA, RetsIn),
        member(RecB, RetsIn),
        name(RecA, NameA),
        name(RecB, NameB),
        NameA @< NameB,
        intersect(RecA, RecB)
    ), Pairs).

% --- READING -----------------------------------------------

% Ensure order of vertices are crescent in one rectangle.
ensure_order(ret(Name, X1, Y1, X2, Y2), ret(Name, X1out, Y1out, X2out, Y2out)) :-
    (X1 =< X2 -> X1out is X1, X2out is X2; X1out is X2, X2out is X1),
    (Y1 =< Y2 -> Y1out is Y1, Y2out is Y2; Y1out is Y2, Y2out is Y1).

% Ensure order of vertices are crescent in a list of rectangles.
ensure_all_order(RetsIn, RetsOut) :- maplist(ensure_order, RetsIn, RetsOut).

% --- MAIN --------------------------------------------------

topo :-
    read(Raw),
    ensure_all_order(Raw, Rets),
    all_intersections(Rets, Pairs),
    length(Pairs, Number),
    writeln(Number),
    forall(member((RecA, RecB), Pairs), (
        write(RecA), write(" "), writeln(RecB)
    )).
